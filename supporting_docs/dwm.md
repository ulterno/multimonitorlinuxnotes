# Other Requirements
- I used `dwm` as the window manager.
- `dmenu` and `st` to make it easier to use dwm.
- I got the sources from https://git.suckless.org/ and compiled and installed them

# How to compile and install from source
1. Create a working directory for yourself and open a terminal there
2. `git clone <git-url>` to get the code
3. `cd` into the source code directory just created.
4. Check the directory...
    1. There should be a Makefile
    2. Check the code if you can
5. Run `make` and then `make install`
    - You might need to elevate privileges for `make install` depending upon where the installation target directory is.

# Elevate Privileges
Read up on `sudo`